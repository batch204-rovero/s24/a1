const getCube = 123 ** 3;
console.log(`The cube of 123 ** 3 is ${getCube}`);

const address = ["Blk2 Lt1", "Casimiro Townhomes 3", "Las Piñas City"];
const [houseNumber, village, city] = address;

function getFullAddress(address) {
	console.log(`I live in ${houseNumber} ${village}, ${city}.`);
}

getFullAddress();

const animal = {
	animalName: "Clifford the Big Red Dog",
	animalBreed: "Giant Vizsla",
	animalWeight: "87 tons",
	animalHeight: "25 feet tall"
};

const {animalName, animalBreed, animalWeight, animalHeight} = animal;
console.log(`${animalName} is a ${animalBreed}. He weighed at ${animalWeight} with a height of ${animalHeight}.`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = "Leo";
myDog.age = "6";
myDog.breed = "Shih Tzu";

console.log(myDog);